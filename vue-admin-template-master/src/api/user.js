import request from '@/utils/request'
import qs from 'qs'

export function login(data) {
  return request({
    url: `/bookcity/user/login?name=${data.username}&password=${data.password}`,
    method: 'post'
  })
}

export function getInfo() {
  return request({
    url: '/bookcity/user/getUserInfo',
    method: 'get'
  })
}

export function logout() {
  return request({
    url: '/bookcity/user/logout',
    method: 'post'
  })
}

export default {

  // 首页界面API
  Userlogin(username, password) {
    return request({
      url: `/bookcity/user/login?name=${username}&password=${password}`,
      method: 'post'
    })
  },

  Userlogout() {
    return request({
      url: `/bookcity/user/logout`,
      method: 'post'
    })
  },

  Adminlogin(adminname, password) {
    return request({
      url: `/bookcity/admin/login?name=${adminname}&password=${password}`,
      method: 'post'
    })
  },

  register(data) {
    return request({
      url: '/bookcity/user/register',
      method: 'post',
      data // 传递form表单类型的数据关键字是data
    })
  },

  adminRegister(data) {
    return request({
      url: '/bookcity/user/admin/register',
      method: 'post',
      data
    })
  },

  getPage(currentPage) {
    return request({
      url: '/bookcity/book/getPage/' + currentPage,
      method: 'get'
    })
  },

  getAllCategory() {
    return request({
      url: '/bookcity/category/getAllCategory',
      method: 'post'
    })
  },

  getAllbooks() {
    return request({
      url: '/bookcity/book/getAllbooks',
      method: 'get'
    })
  },

  selectByCondition(input, currentPage) {
    return request({
      url: `/bookcity/book/selectbycondition?condition=${input}&startPage=${currentPage}`,
      method: 'post'
    })
  },

  getbookbyCategory(parentId, subId) {
    return request({
      url: `/bookcity/book/getbookbyCategory/${parentId}/${subId}`,
      method: 'get'
    })
  },

  getBookById(bookId) {
    return request({
      url: `/bookcity/book/getbookbyid/${bookId}`,
      method: 'get'
    })
  },

  // 购物车界面API
  getAllCart() {
    return request({
      url: `/bookcity/shopping-cart/getAllcart`,
      method: 'get'
    })
  },

  addCart(data) {
    return request({
      url: `/bookcity/shopping-cart/addCart`,
      method: 'post',
      data
    })
  },

  cartUpdateNumber(data) {
    return request({
      url: `/bookcity/shopping-cart/updateNumber`,
      method: 'post',
      data
    })
  },

  createOrder(data) {
    return request({
      url: `/bookcity/order/createOrder`,
      method: 'post',
      data
    })
  },

  // 订单管理界面API
  getAllOrder() {
    return request({
      url: `/bookcity/order/getAllOrder`,
      method: 'get'
    })
  },

  orderUpdateStatus(data) {
    return request({
      url: `/bookcity/order/updateStatus/${data}`,
      method: 'post'
    })
  },

  ranking() {
    return request({
      url: '/bookcity/order/getRank',
      method: 'get'
    })
  },

  getEmailCode(email) {
    return request({
      url: '/bookcity/email/getEmailCode/' + email,
      method: 'get'
    })
  },

  deleteCart(data) {
    return request({
      url: '/bookcity/shopping-cart/deletecarts',
      method: 'post',
      data: qs.stringify({ bookIds: data }, { indices: false })
    })
  },

  deleteOrders(data) {
    return request({
      url: '/bookcity/order/deleteOrder',
      method: 'post',
      data: qs.stringify({ orderIds: data }, { indices: false })
    })
  },

  modifyPassword(data) {
    return request({
      url: '/bookcity/user/modifyPassword',
      method: 'post',
      data
    })
  },

  getInventories() {
    return request({
      url: '/bookcity/inventory/getInventories',
      method: 'get'
    })
  },

  addInventories(data) {
    return request({
      url: '/bookcity/inventory/addInventories',
      method: 'post',
      data
    })
  },

  getInventoriesByIds(params) {
    return request({
      url: `/bookcity/inventory/getInventoriesByIds?ids=${params}`,
      method: 'get'
    })
  },

  getInventoriesLikeName(data) {
    return request({
      url: `/bookcity/inventory/getInventoriesLikeName?name=${data}`,
      method: 'get'
    })
  },

  getNotEnoughInventories(begin, end) {
    return request({
      url: `/bookcity/inventory/getNotEnoughInventories?from=${begin}&to=${end}`,
      method: 'get'
    })
  },

  submitApply(ruleForm) {
    return request({
      url: `/bookcity/bookPurchaseApply/submitApply`,
      method: 'post',
      data: ruleForm
    })
  },

  pickUpTask(applyId, userId) {
    return request({
      url: `/bookcity/bookPurchaseApply/pickUpTask?applyId=${applyId}&userId=${userId}`,
      method: 'post'
    })
  },

  completeTask(applyId, isPass, content) {
    return request({
      url: `/bookcity/bookPurchaseApply/completeTask?applyId=${applyId}&${content ? 'content=' + content : content}&isPass=${isPass}`,
      method: 'post'
    })
  },

  getUsersInfo() {
    return request({
      url: `/bookcity/user/getUsersInfo`,
      method: 'get'
    })
  },

  modifyUserRole(role, id) {
    return request({
      url: `/bookcity/user/modifyUserRole?newRole=${role}&userId=${id}`,
      method: 'post'
    })
  },

  addRole(name) {
    return request({
      url: `/bookcity/role/addRole?roleName=${name}`,
      method: 'post'
    })
  },

  getAllRoles() {
    return request({
      url: `/bookcity/role/getAllRoles`,
      method: 'get'
    })
  },

  updateRoleByName(newName, oldName) {
    return request({
      url: `/bookcity/role/updateRoleByName?newName=${newName}&oldName=${oldName}`,
      method: 'post'
    })
  },

  addFirstCategory(data) {
    return request({
      url: `/bookcity/category/addfirstCategory`,
      method: 'post',
      data
    })
  },
  addSecondCategory(data) {
    return request({
      url: `/bookcity/category/addsecondCategory`,
      method: 'post',
      data
    })
  },

  deleteFirstCategory(id) {
    return request({
      url: `/bookcity/category/deletesecondCategory/${id}`,
      method: 'post'
    })
  },

  deleteSecondCategory(id) {
    return request({
      url: `/bookcity/category/deletefirstCategory/${id}`,
      method: 'post'
    })
  },

  deleteBookById(bookId) {
    return request({
      url: `/bookcity/book/deletebook/${bookId}`,
      method: 'get'
    })
  },

  uploadBooks(data) {
    return request({
      url: `/bookcity/book/excel/uploadbooks`,
      method: 'post',
      data
    })
  },

  updateBook(data) {
    return request({
      url: `/bookcity/book/updatebook`,
      method: 'post',
      data
    })
  },

  newAddBook(data) {
    return request({
      url: `/bookcity/book/newaddbook`,
      method: 'post',
      data
    })
  },

  uploadPics(data) {
    return request({
      url: `/bookcity/book/upload`,
      method: 'post',
      data
    })
  },

  download(begin, name, end) {
    return request({
      url: `/bookcity/book/excel/download?from=${begin}&nameCondition=${name}&to=${end}`,
      method: 'get',
      responseType: 'blob'
    })
  },

  downloadAll() {
    return request({
      url: `/bookcity/book/excel/download`,
      method: 'get',
      responseType: 'blob'
    })
  }
}

