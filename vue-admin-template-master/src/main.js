import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/en' // lang i18n

import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'
import canvas from './js/canvas'
import './assets/css/normalize.css'
import './assets/css/orderTime.css'
import TreeTable from 'vue-table-with-tree-grid'
Vue.component('TreeTable', TreeTable)
import '@/icons' // icon
import '@/permission' // permission control

/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online ! ! !
 */
if (process.env.NODE_ENV === 'production') {
  const { mockXHR } = require('../mock')
  mockXHR()
}

// set ElementUI lang to EN
Vue.use(ElementUI, { locale })
// 如果想要中文版 element-ui，按如下方式声明
// Vue.use(ElementUI)
Vue.component('TreeTable', TreeTable)
Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})

import axios from 'axios'
import qs from 'qs'
import * as echarts from 'echarts'

axios.defaults.baseURL = 'http://localhost:8080'
Vue.prototype.$http = axios
Vue.prototype.$echarts = echarts
Vue.prototype.$qs = qs
Object.defineProperty(Vue.prototype, '$moment', { value: canvas })
// 消除element中按钮的小bug  手动blur
Vue.prototype.clickHandler = function(evt) {
  let target = evt.target
  if (target.nodeName === 'SPAN') {
    target = evt.target.parentNode
  }
  target.blur()
}

