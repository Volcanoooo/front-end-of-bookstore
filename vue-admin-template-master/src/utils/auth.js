import Cookies from 'js-cookie'

const TokenKey = 'vue_admin_template_token'
const SessionKey = 'Authorization'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function getSession() {
  return Cookies.get(SessionKey)
}

export function setSession(session) {
  return Cookies.set(SessionKey, session)
}

export function removeSession() {
  return Cookies.remove(SessionKey)
}
