import Vue from 'vue'
import Router from 'vue-router'

// 自定义组件
import Home from '@/views/admin/Home'
import PanelChild from '@/views/admin/PanelChild'
import Classify from '@/views/admin/Classify'
import BookManage from '@/views/admin/BookManage'
import BookSearch from '@/views/admin/BookSearch'
import BookAdd from '@/views/admin/BookAdd'
import OrderManagement from '@/views/admin/OrderManagement'
import OrderApply from '@/views/admin/OrderApply'
import OrderDeploy from '@/views/admin/OrderDeploy'
import OrderCheck from '@/views/admin/OrderCheck'
import BookUpdate from '@/views/admin/BookUpdate'
import UpLoadExcel from '@/views/admin/UpLoadExcel'
import NewUserApplication from '@/views/admin/NewUserApplication'
import SubmitApply from '@/views/admin/SubmitApply'
import PickUp from '@/views/admin/PickUp'
import WaitAudit from '@/views/admin/WaitAudit'
import ProcessManage from '@/views/admin/ProcessManage'

Vue.use(Router)

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/404',
    name: '404',
    component: () => import('@/views/404'),
    hidden: true
  },
  {
    path: '/bookstore',
    component: () => import('@/views/user/index'),
    name: 'index',
    meta: { title: '知遇书堂' }
  },
  {
    path: '/userLogin',
    component: () => import('@/views/login/Login'),
    name: 'userLogin',
    meta: { title: '登录' }
  },
  {
    path: '/register',
    component: () => import('@/views/login/register'),
    name: 'Register',
    meta: { title: '账户注册' }
  },
  {
    path: '/modifyPassword',
    component: () => import('@/views/login/modifyPassword'),
    name: 'modifyPassword',
    meta: { title: '修改密码' }
  },
  {
    path: '/',
    component: () => import('@/views/user/index'),
    name: 'bookstore',
    meta: { title: '知遇书堂' }
  },
  {
    path: '/bookDetails',
    component: () => import('@/views/user/bookDetails'),
    name: 'bookDetails',
    meta: { title: '图书详情' }
  }

]

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const asyncRoutes = [{
  path: '/shoppingCart',
  component: () => import('@/views/user/shoppingCart'),
  name: 'shoppingCart',
  meta: { title: '我的购物车', roles: ['admin', 'manager', 'financial', 'worker', 'user'] }
},
{
  path: '/ordersManage',
  component: () => import('@/views/user/ordersManage'),
  name: 'ordersManage',
  meta: { title: '订单管理', roles: ['admin', 'manager', 'financial', 'worker', 'user'] }
},
{
  path: '/panel',
  name: '数据预览',
  component: Home,
  show: true,
  meta: { title: '管理页面', roles: ['admin', 'manager', 'financial', 'worker'] },
  children: [
    {
      path: '/panelChild',
      name: '销量数据',
      component: PanelChild,
      iconCls: 'el-icon-odometer',
      meta: { title: '销量数据', roles: ['admin', 'manager', 'financial', 'worker'] }
    }
  ]
},
{
  path: '/home',
  name: '管理模块',
  component: Home,
  redirect: '/classify',
  // show是否被遍历
  show: true,
  meta: { title: '管理模块', roles: ['admin', 'manager', 'financial', 'worker'] },
  children: [
    {
      path: '/classify',
      name: '分类管理',
      component: Classify,
      iconCls: 'el-icon-notebook-2',
      meta: { title: '分类管理', roles: ['admin', 'manager', 'financial', 'worker'] }
    },
    {
      path: '/processManage',
      name: '流程管理',
      component: ProcessManage,
      iconCls: 'el-icon-collection-tag',
      meta: { title: '流程管理', roles: ['admin'] },
      redirect: '/submitApply',
      children: [{
        path: '/submitApply',
        name: '购书申请',
        component: SubmitApply,
        meta: { title: '购书申请', roles: ['admin'] }
      },
      {
        path: '/managerPickUp',
        name: '经理拾取任务',
        component: PickUp,
        meta: { title: '经理拾取任务', roles: ['admin'] }
      },
      {
        path: '/managerAudit',
        name: '经理审核',
        component: WaitAudit,
        meta: { title: '经理审核', roles: ['admin'] }
      }, {
        path: '/financialPickUp',
        name: '财务拾取任务',
        component: PickUp,
        meta: { title: '财务拾取任务', roles: ['admin'] }
      },
      {
        path: '/financialAudit',
        name: '财务审核',
        component: WaitAudit,
        meta: { title: '财务审核', roles: ['admin'] }
      }
      ]
    },
    { path: '/bookManage', name: '图书管理', component: BookManage, iconCls: 'el-icon-chat-round',
      redirect: '/bookAdd',
      meta: { title: '图书管理', roles: ['admin', 'manager', 'financial', 'worker'] },
      children: [
        {
          path: '/bookAdd',
          name: '添加图书',
          component: BookAdd,
          meta: { title: '添加图书', roles: ['admin', 'manager', 'financial', 'worker'] }
        },
        {
          path: '/bookSearch',
          name: '搜索显示',
          component: BookSearch,
          meta: { title: '搜索显示', roles: ['admin', 'manager', 'financial', 'worker'] }
        }
      ] },
    {
      path: '/upLoadExcel',
      name: '上传文件',
      component: UpLoadExcel,
      iconCls: 'el-icon-upload',
      meta: { title: '上传文件', roles: ['admin', 'manager', 'financial', 'worker'] }
    },
    {
      path: '/downLoadExcel',
      name: '下载文件',
      component: () => import('@/views/admin/downLoadExcel'),
      iconCls: 'el-icon-download',
      meta: { title: '下载文件', roles: ['admin', 'manager', 'financial', 'worker'] }
    }
  ]
},
{
  path: '/systemManage',
  name: '订单管理',
  component: Home,
  show: true,
  meta: { title: '订单管理', roles: ['admin', 'manager', 'financial', 'worker'] },
  children: [
    {
      path: '/orderManagement',
      name: '订单管理',
      component: OrderManagement,
      iconCls: 'el-icon-collection-tag',
      meta: { title: '订单管理', roles: ['admin', 'manager', 'financial', 'worker'] }
    },
    {
      path: '/orderApply',
      name: '订单申请',
      component: OrderApply,
      iconCls: 'el-icon-ice-cream-round',
      meta: { title: '订单申请', roles: ['admin', 'manager', 'financial', 'worker'] }
    },
    {
      path: '/orderDeploy',
      name: '订单部署',
      component: OrderDeploy,
      iconCls: 'el-icon-edit',
      meta: { title: '订单部署', roles: ['admin', 'manager', 'financial', 'worker'] }
    },
    {
      path: '/orderCheck',
      name: '订单查看',
      component: OrderCheck,
      iconCls: 'el-icon-ship',
      meta: { title: '订单查看', roles: ['admin', 'manager', 'financial', 'worker'] }
    },
    {
      path: '/newUserApplication',
      name: '用户新建申请',
      component: NewUserApplication,
      iconCls: 'el-icon-ship',
      meta: { title: '订单查看', roles: ['admin', 'manager', 'financial', 'worker'] }
    }
  ]
},
{
  path: '/bookUpdate',
  component: Home,
  show: false,
  meta: { title: '图书修改', roles: ['admin', 'manager', 'financial', 'worker'] },
  children: [
    {
      path: '/BookUpdate',
      name: '图书管理里的修改',
      component: BookUpdate,
      meta: { title: '图书修改', roles: ['admin', 'manager', 'financial', 'worker'] }
    }
  ]
},
{
  path: '/bookSearch',
  component: Home,
  show: false,
  meta: { title: '搜索', roles: ['admin', 'manager', 'financial', 'worker'] },
  children: [
    {
      path: '/BookSearch',
      name: '搜索显示',
      component: BookSearch,
      meta: { title: '搜索', roles: ['admin', 'manager', 'financial', 'worker'] }
    }
  ]
},
{
  path: '/inventory',
  name: '库存系统',
  component: Home,
  show: true,
  meta: { title: '库存系统', roles: ['admin', 'worker'] },
  children: [
    {
      path: '/inventoryManage',
      name: '库存管理',
      component: () => import('@/views/admin/inventoryManage'),
      iconCls: 'el-icon-s-data',
      meta: { title: '库存管理', roles: ['admin', 'worker'] }
    }
  ]
},
{
  path: '/permission',
  name: '权限系统',
  component: Home,
  show: true,
  meta: { title: '权限系统', roles: ['admin'] },
  children: [
    {
      path: '/userManage',
      name: '用户管理',
      component: () => import('@/views/admin/userManage'),
      iconCls: 'el-icon-s-custom',
      meta: { title: '用户管理', roles: ['admin'] }
    },
    {
      path: '/rolesManage',
      name: '角色管理',
      component: () => import('@/views/admin/rolesManage'),
      iconCls: 'el-icon-s-check',
      meta: { title: '角色管理', roles: ['admin'] }
    }
  ]
},
// 404 page must be placed at the end !!!
{ path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
